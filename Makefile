.PHONY : all
all: appsboot.mbn

appsboot.mbn :
	cd boot && $(MAKE)
	cd ../legacy && $(MAKE)
	cd boot/tools && $(MAKE)
	./boot/tools/mkheader-8x50 ../legacy/usbloader/usbloader appsboothd.mbn
	cat appsboothd.mbn ../legacy/usbloader/usbloader > $@

.PHONY : clean
clean:
	rm -rf appsboot.mbn appsboothd.mbn
	cd boot && $(MAKE) clean
	cd ../legacy && $(MAKE) clean
	cd boot/tools && $(MAKE) clean
